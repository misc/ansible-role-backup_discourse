#!/usr/bin/ruby

require 'discourse_api'

def download_tarball(host, username, key, filename)

    connection_options ||= {
        url: host,
        headers: {
            accept: 'application/json',
        }
    }

    connection ||= Faraday.new connection_options do |conn|
        # Follow redirects
        conn.use FaradayMiddleware::FollowRedirects, limit: 5
        # Convert request params to "www-form-encoded"
        conn.request :url_encoded
        # Use Faraday's default HTTP adapter
        conn.adapter Faraday.default_adapter
        #pass api_key and api_username on every request
        conn.params['api_key'] = key
        conn.params['api_username'] = username
    end

    response = connection.send(:get, '/admin/backups/' + filename )
    response.env

    f = File.open(filename,'w')
    f.write(response.env[:body])
    f.close()
end

unless ARGV.length == 4  
  puts "Usage: ruby backup_discourse.rb website username key number_of_backups"  
  exit 1
end 

website = ARGV[0]
username = ARGV[1]
key = ARGV[2]
backup_number = ARGV[3] 

client = DiscourseApi::Client.new(website)
client.api_key = key
client.api_username = username

backups = []

response = client.get('/admin/backups')
if response[:body].has_key?("errors") then
  puts "error: " + response[:body]["errors"][0]
  exit
end

response[:body].each do |i|
  backups << i['filename']
end

backups.sort!
to_download = backups.last(5)
to_download.each do |f|
    if not File.exist?(f) then
        # download
        download_tarball(website, username, key, f)
    end
end
